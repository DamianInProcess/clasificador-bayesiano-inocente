from pyspark import SparkContext
from pyspark.sql import Row
from pyspark.sql import SQLContext


import math

def isInglish(x):
    from nltk.corpus import stopwords
    res = False
    ratios  = {}
    tokens = x.split(" ")
    words = [word.lower() for word in tokens]

    for language in stopwords.fileids():
        stopwords_idioma = set(stopwords.words(language))
        words_conjunto = set(words)
        elementos_comunes = words_conjunto.intersection(stopwords_idioma)
        ratios[language] = len(elementos_comunes)

    if max(ratios, key= ratios.get) == 'english':
        res = True

    return res

def estructura(x):
    y = x.split("', u'")
    y[0] = y[0].replace("(u'","")
    y[1] = y[1].replace("')","")
    return Row(
        genero = y[0],
        letra = y[1]
    )

def estructuraTR(x):
    y = x.split("', ")
    y[0] = y[0].replace("(u'", "")
    y[1] = y[1].replace("u'", "")
    y[2] = y[2].replace(")", "")
    return Row(
        GENERO = y[0],
        PALABRA = y[1],
        TFIDF = float(y[2])
    )

def contar(letra):
    from nltk.stem.lancaster import LancasterStemmer
    from nltk.corpus import stopwords
    st = LancasterStemmer()
    resTmp = []
    res = []
    listaNegra = []
    letra = letra.split(" ")
    stop = set(stopwords.words('english'))
    for i, word in enumerate(letra):
        cont = 1
        word = st.stem(word.lower())
        if len(word) > 2 and word not in stop and word not in listaNegra:
            for other in letra[i + 1:]:
                if other == word:
                    cont += 1
            resTmp.append((word, float(cont)))
            listaNegra.append(word)

    for word in sorted(resTmp, key=lambda numb: numb[1], reverse=True)[:]:
        res.append(word[0])
    return res

def tfidf(k, vs):
    return math.log(float(dic[k]) / len(vs)) * sum(vs)

def registraTabla():

    rddTR = sc.textFile("tablaRasa.txt").map(lambda x: estructuraTR(x))

    shemaFinal20 = sqlContx.createDataFrame(rddTR)

    shemaFinal20.registerTempTable("FINAL")


def predictor(letra):
    maximo = 0
    ganador = ''

    letraD = contar(letra)

    sql = "SELECT GENERO,(ROUND(EXP(SUM(LOG(TFIDF*10))),1)) AS TOTALES FROM FINAL WHERE PALABRA = '" + str(letraD[0]) + "' "
    for word in letraD[1:]:
        sql = sql + "OR PALABRA = '" + str(word) + "' "

    sql = sql + "GROUP BY GENERO"

    df2 = sqlContx.sql(sql)

    rddAux = df2.rdd.map(lambda x: (x.GENERO, (float(dic[x.GENERO]) / float(letrasTotal)) * x.TOTALES))

    for x in rddAux.toLocalIterator():
        if maximo < x[1]:
            maximo = x[1]
            ganador = x[0]
    return ganador
####################################################################################################################################################################################################

sc = SparkContext(appName="tablaRasa")

sqlContx = SQLContext(sc)

dic = {
        "R&B": 2693,
        "Indie": 2415,
        "Metal": 17975,
        "Electronic": 5941,
        "Jazz": 5939,
        "Hip-Hop": 18495,
        "Country": 11474,
        "Folk": 1525,
        "Pop": 28168,
        "Rock": 82187
    }

registraTabla()

letrasTotal = sum(y for x, y in dic.items())

rdd = sc.textFile("Filtro20.txt")

rddPres = rdd.map(lambda x : estructura(x))\
            .filter(lambda x :isInglish(x.letra))

print (rddPres.first().genero,predictor(rddPres.first().letra))
