from pyspark import SparkContext
from pyspark.sql import Row
from pyspark.sql import SQLContext


dic = {
        "R&B": 2693,
        "Indie": 2415,
        "Metal": 17975,
        "Electronic": 5941,
        "Jazz": 5939,
        "Hip-Hop": 18495,
        "Country": 11474,
        "Folk": 1525,
        "Pop": 28168,
        "Rock": 82187
    }

def isInglish(x):
    from nltk.corpus import stopwords
    res = False
    ratios  = {}
    tokens = x.split(" ")
    words = [word.lower() for word in tokens]

    for language in stopwords.fileids():
        stopwords_idioma = set(stopwords.words(language))
        words_conjunto = set(words)
        elementos_comunes = words_conjunto.intersection(stopwords_idioma)
        ratios[language] = len(elementos_comunes)

    if max(ratios, key= ratios.get) == 'english':
        res = True

    return res


def estructura(x):
    y = x.split("', u'")
    y[0] = y[0].replace("(u'","")
    y[1] = y[1].replace("')","")
    return Row(
        genero = y[0],
        letra = y[1]
    )

sc = SparkContext(appName="ContGeneros")

sqlContx = SQLContext(sc)

rddFianal80a = sc.textFile("Filtro80.txt").map(lambda x: estructura(x)).filter(lambda x : isInglish(x.letra))

shemaFinal20 = sqlContx.createDataFrame(rddFianal80a)

shemaFinal20.registerTempTable("FINAL")

rddFianal80a = rddFianal80a.map(lambda x : (x.genero, x.letra))

rddFianal80a.saveAsTextFile("/home/damian/PycharmProjects/untitled/Final80.txt")

res = []

for genero in dic:
    sentencia = sqlContx.sql("SELECT COUNT(*) FROM FINAL WHERE genero = '"+genero+"'")
    res.append((genero, sentencia.first()))

for x in res:
    print x