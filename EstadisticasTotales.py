from pyspark import SparkContext
from pyspark.sql import Row
from pyspark.sql import SQLContext

import math


def isInglish(x):
    from nltk.corpus import stopwords
    res = False
    ratios = {}
    tokens = x.split(" ")
    words = [word.lower() for word in tokens]

    for language in stopwords.fileids():
        stopwords_idioma = set(stopwords.words(language))
        words_conjunto = set(words)
        elementos_comunes = words_conjunto.intersection(stopwords_idioma)
        ratios[language] = len(elementos_comunes)

    if max(ratios, key=ratios.get) == 'english':
        res = True

    return res


def estructura(x):
    y = x.split("', u'")
    y[0] = y[0].replace("(u'", "")
    y[1] = y[1].replace("')", "")
    return Row(
        genero=y[0],
        letra=y[1]
    )


def estructuraTR(x):
    y = x.split("', ")
    y[0] = y[0].replace("(u'", "")
    y[1] = y[1].replace("u'", "")
    y[2] = y[2].replace(")", "")
    return Row(
        GENERO=y[0],
        PALABRA=y[1],
        TFIDF=float(y[2])
    )


def contar(letra):
    from nltk.stem.lancaster import LancasterStemmer
    from nltk.corpus import stopwords
    st = LancasterStemmer()
    resTmp = []
    res = []
    listaNegra = []
    letra = letra.split(" ")
    stop = set(stopwords.words('english'))
    for i, word in enumerate(letra):
        cont = 1
        word = st.stem(word.lower())
        if len(word) > 2 and word not in stop and word not in listaNegra:
            for other in letra[i + 1:]:
                if other == word:
                    cont += 1
            resTmp.append((word, float(cont)))
            listaNegra.append(word)

    for word in sorted(resTmp, key=lambda numb: numb[1], reverse=True)[:]:
        res.append(word[0])
    return res


def tfidf(k, vs):
    return math.log(float(dic[k]) / len(vs)) * sum(vs)


def registraTabla():
    rddTR = sc.textFile("tablaRasa.txt").map(lambda x: estructuraTR(x))

    shemaFinal20 = sqlContx.createDataFrame(rddTR)

    shemaFinal20.registerTempTable("FINAL")


def predictor(generoOriginal, letra, indice):
    res_fase1 = []

    for palabraOrigen in letra:
        for genero, palabra, TFIDF in tablaRasa.value:
            if palabraOrigen == palabra:
                res_fase1.append((genero + "_" + str(indice), (generoOriginal, TFIDF)))

    return res_fase1


####################################################################################################################################################################################################

sc = SparkContext(appName="tablaRasa")

sqlContx = SQLContext(sc)

dic = {
    "R&B": 2693,
    "Indie": 2415,
    "Metal": 17975,
    "Electronic": 5941,
    "Jazz": 5939,
    "Hip-Hop": 18495,
    "Country": 11474,
    "Folk": 1525,
    "Pop": 28168,
    "Rock": 82187
}

letrasTotal = sum(y for x, y in dic.items())

tablaRasa = sc.broadcast(sc.textFile("tablaRasa.txt").map(lambda x: estructuraTR(x)).collect())

dicBroad = sc.broadcast(dic)

letrasTBrad = sc.broadcast(letrasTotal)

rdd = sc.textFile("Filtro20.txt")

rdd =  sc.parallelize(rdd.collect())


def mul(a, b):
    return (a[0], a[1] * b[1])

def id(x):
    genero_id = x[0].split("_")
    total = float(x[1][1]) * (float(dicBroad.value[genero_id[0]]) / float(letrasTBrad.value))
    return (genero_id[1],(genero_id[0],x[1][0], total))

def mayor(a, b):
    if a[2] > b[2]:
        return (a)
    else:
        return (b)


def aciertos(x):
    if x[1][0] == x[1][1]:
        return (x[1][1], 1)
    else:
        return (x[1][1], 0)


rddPres = rdd.map(lambda x: estructura(x)) \
    .filter(lambda x: isInglish(x.letra)) \
    .map(lambda x: (x.genero, contar(x.letra))) \
    .zipWithIndex() \
    .flatMap(lambda x: predictor(x[0][0], x[0][1], x[1]))\
    .reduceByKey(lambda a, b: mul(a, b))\
    .map(lambda x : id(x))\
    .reduceByKey(lambda a, b: mayor(a,b))\
    .map(lambda x : aciertos(x))\
    .reduceByKey(lambda a, b: a+b)

print(rddPres.collect())

rddPres.saveAsTextFile("Resultado.txt")
