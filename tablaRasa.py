from pyspark import SparkContext
from pyspark.sql import Row

import math

sc = SparkContext(appName="tablaRasa")

def estructura(x):
    y = x.split("', u'")
    y[0] = y[0].replace("(u'","")
    y[1] = y[1].replace("')","")
    return Row(
        genero = y[0],
        letra = y[1]
    )

def contar(letra):
    from nltk.stem.lancaster import LancasterStemmer
    from nltk.corpus import stopwords
    st = LancasterStemmer()
    res = []
    listaNegra = []
    letra = letra.split(" ")
    total = len(letra)
    stop = set(stopwords.words('english'))
    for i, word in enumerate(letra):
        cont = 1
        word = st.stem(word.lower())
        if len(word) > 2 and word not in stop and word not in listaNegra:
            for other in letra[i + 1:]:
                if other == word:
                    cont += 1
            res.append((word, float(cont) / float(total)))
            listaNegra.append(word)

    return sorted(res, key=lambda numb: numb[1], reverse=True)[:]

def tfidf(k, vs):
    dic = {
        "R&B": 2693,
        "Indie": 2415,
        "Metal": 17975,
        "Electronic": 5941,
        "Jazz": 5939,
        "Hip-Hop": 18495,
        "Country": 11474,
        "Folk": 1525,
        "Pop": 28168,
        "Rock": 82187
    }

    return math.log(float(dic[k]) / len(vs)) * sum(vs)


rddL = sc.textFile("/home/damian/PycharmProjects/untitled/Final80.txt").map(lambda x : estructura(x))

rddL = rddL.map(lambda x: (x.genero, contar(x.letra)))

rddL2 = rddL.flatMap(lambda x: (((x[0], k[0]), k[1]) for k in x[1]))

rddL3 = rddL2.groupByKey().map(lambda (k,v): (k[0],k[1],tfidf(k[0],v)))

rddL3.saveAsTextFile("tablaRasa.txt")


